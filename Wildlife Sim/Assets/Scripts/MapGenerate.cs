﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerate : MonoBehaviour
{
    //public GameObject playerCamera;

    public List<List<Tile>> allHere = new List<List<Tile>>();

    public Biomes addBiomes;

    int size = 100;
    int biomeSize = 9;
    int planeSize = 1;
    List<GridPos> gridDistorted = new List<GridPos>();
    //public List<List<Node>> grid = new List<List<Node>>();
    //public List<Node> grid = new List<Node>();

    public Graph graph;
    public Controll controll;

    [System.Serializable]
    public struct Biomes
    {
        public List<Tiles> biomes;
    }
    [System.Serializable]
    public struct Tiles
    {
        public List<Tile> tiles;
        public int amount;
    }
    [System.Serializable]
    public struct Tile
    {
        public GameObject gObject;
        public int amount;
        public float canWalk;
        public Tile(GameObject gObjectT, float canWalkT)
        {
            amount = 1;
            gObject = gObjectT;
            canWalk = canWalkT;
        }
    }

    public struct GridPos
    {
        public Vector3 position;
        public List<Tile> biomes;

        public GridPos(Vector3 vT, List<Tile> bT)
        {
            position = vT;
            biomes = bT;
        }
    }

    void Start()
    {
        controll = this.GetComponent<Controll>();
        graph = new Graph();

        int biomelistcount = -1;
        for (int i = 0; i < addBiomes.biomes.Count; i++)
        {
            for (int j = 0; j < addBiomes.biomes[i].amount; j++)
            {
                allHere.Add(new List<Tile>());
                biomelistcount++;
                for (int k = 0; k < addBiomes.biomes[i].tiles.Count; k++)
                {
                    for (int l = 0; l < addBiomes.biomes[i].tiles[k].amount; l++)
                    {
                        allHere[biomelistcount].Add(new Tile(addBiomes.biomes[i].tiles[k].gObject, addBiomes.biomes[i].tiles[k].canWalk));
                    }
                }
            }
        }

        Generate();
        GenerateObject(transform.GetChild(0), Resources.Load("Prefabs/Rabbit") as GameObject, true, 1f, 150f, 0.72f, 7f, 1, 3); // Rabbits
        GenerateObject(transform.GetChild(0), Resources.Load("Prefabs/Fox") as GameObject, true, 2f, 170f, 0.8f, 7f, 1, 1); // Foxes
        GenerateObject(transform.GetChild(0), Resources.Load("Prefabs/Grass 2") as GameObject, true, 0.7f, 50f, 0.72f, 7f, 3, 5); // Grass in big patches
        GenerateObject(transform.GetChild(0), Resources.Load("Prefabs/Grass 2") as GameObject, true, 10f, 100f, 0.72f, 7f, 2, 4); // Grass sparse
        GenerateObject(transform.GetChild(0), Resources.Load("Prefabs/Oak_Tree") as GameObject, false, 1f, 130f, 0.72f, 7f, 1, 3); // Trees
        graph.CreateAStarGrid();
        StartCoroutine(SpawnOverTime(Constants.grassSpawnRate, Resources.Load("Prefabs/Grass 2") as GameObject));
    }

    void Update()
    {
        //graph.debugDraw();
    }

    void Generate()
    {
        for (int i = 0; i < size / biomeSize; i++)
        {
            for (int j = 0; j < size / biomeSize; j++)
            {
                int randomX = Random.Range(-planeSize * biomeSize / 2, planeSize * biomeSize / 2);
                int randomZ = Random.Range(-planeSize * biomeSize / 2, planeSize * biomeSize / 2);
                int randomBiome = Random.Range(0, allHere.Count);
                gridDistorted.Add(new GridPos(new Vector3(i * planeSize * biomeSize + randomX, 0, j * planeSize * biomeSize + randomZ), allHere[randomBiome]));
            }
        }

        int randomTile;
        for (int i = 0; i < size; i++)
        {
            if (graph.nodes == null)
                graph.nodes = new List<List<Node>>();
            graph.nodes.Add(new List<Node>());
            for (int j = 0; j < size; j++)
            {
                GridPos closest = gridDistorted[i];
                foreach (GridPos g in gridDistorted)
                {
                    if ((g.position - new Vector3(i * planeSize, 0, j * planeSize)).magnitude < (closest.position - new Vector3(i * planeSize, 0, j * planeSize)).magnitude)
                        closest = g;
                }
                randomTile = Random.Range(0, closest.biomes.Count);
                graph.nodes[i].Add(new Node(Instantiate(closest.biomes[randomTile].gObject, new Vector3(i * planeSize, 0, j * planeSize), Quaternion.identity), i, j, closest.biomes[randomTile].canWalk));
            }
        }
        //playerCamera.transform.position = new Vector3((size * planeSize) / 2, 0, (size * planeSize) / 2);
    }

    void GenerateObject(Transform spawnOn, GameObject Obj, bool canWalk, float PatchSmallness, float Sparseness, float Gap, float EdgeBlur, int MinSpawn, int MaxSpawn)
    {
        List<Vector3> returned;
        Node toSpawn;

        returned = PerlinNoise.GenerateObject(spawnOn, PatchSmallness, Sparseness, Gap, EdgeBlur, MinSpawn, MaxSpawn);
        foreach (Vector3 V in returned)
        {
            GameObject on = Functions.isGrounded2(V);
            if (Functions.isGrounded(V, 0.2f))
            {
                toSpawn = graph.FindNode(on);
                if (toSpawn != null && toSpawn.objectOn == false && (Obj.name != "Grass 2" || toSpawn.canWalk == 1))
                {
                    toSpawn.objectOn = true;
                    toSpawn.canWalk = (canWalk ? toSpawn.canWalk : 0);
                    GameObject temp = Instantiate(Obj, on.transform.position, spawnOn.rotation);
                }
            }
        }
    }

    public void AddFox()
    {
        Node temp = graph.nodes[Random.Range(0, size)][Random.Range(0, size)];
        while (temp.canWalk == 0 || temp.objectOn == true)
        {
            temp = graph.nodes[Random.Range(0, size)][Random.Range(0, size)];
        }
        Instantiate(Resources.Load("Prefabs/Fox") as GameObject, temp.gObject.transform.position, temp.gObject.transform.rotation);
    }
    public void AddRabbit()
    {
        Node temp = graph.nodes[Random.Range(0, size)][Random.Range(0, size)];
        while (temp.canWalk == 0 || temp.objectOn == true)
        {
            temp = graph.nodes[Random.Range(0, size)][Random.Range(0, size)];
        }
        Instantiate(Resources.Load("Prefabs/Rabbit") as GameObject, temp.gObject.transform.position, temp.gObject.transform.rotation);
    }

    IEnumerator SpawnOverTime(float spawnRate, GameObject Obj)
    {
        while (true)
        {
            Node temp = graph.nodes[Random.Range(0, size)][Random.Range(0, size)];
            while (temp.canWalk < 1 || temp.objectOn == true)
            {
                temp = graph.nodes[Random.Range(0, size)][Random.Range(0, size)];
            }
            Instantiate(Obj, temp.gObject.transform.position, temp.gObject.transform.rotation);
            yield return new WaitForSeconds(spawnRate);
        }
    }
}
