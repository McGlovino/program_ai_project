﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerlinNoise
{
    static int width = 256;
    static int height = 256;

    public static List<Vector3> GenerateObject(Transform plane, float patchSmallness, float sparseness, float gap, float edgeBlur, float minSpawn, float maxSpawn)
    {
        List<Vector3> toReturn = new List<Vector3>();

        List<Vector3> positions = PerlinNoise.Generate(plane.transform, patchSmallness, sparseness, gap);

        foreach (Vector3 v in positions)
        {
            float minX = v.x - edgeBlur;
            float maxX = v.x + edgeBlur;
            float minZ = v.z - edgeBlur;
            float maxZ = v.z + edgeBlur;

            for (int i = 0; i < Random.Range(minSpawn, maxSpawn + 1); i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
                if (Functions.isGrounded(spawnPos))
                {
                    toReturn.Add(spawnPos);
                }
            }
        }
        return toReturn;
    }

    static List<Vector3> Generate(Transform plane, float patchSmallness, float sparseness, float gap)
    {
        List<Vector3> toReturn = new List<Vector3>();

        Texture2D texture = GenerateTexture(patchSmallness * plane.localScale.x, patchSmallness * plane.localScale.z, width, height);
        float minX = plane.position.x - plane.localScale.x * 10 / 2;
        float minZ = plane.position.z - plane.localScale.z * 10 / 2;

        int fractionX = (int)(256 / sparseness * (plane.localScale.x));
        int fractionZ = (int)(256 / sparseness * (plane.localScale.z));

        for (int x = 1; x < fractionX; x++)
        {
            for (int z = 1; z < fractionZ; z++)
            {
                if (texture.GetPixel((int)(256 * ((float)x / (float)fractionX)), (int)(256 * ((float)z / (float)(fractionZ)))).r > gap)
                    toReturn.Add(new Vector3(minX + (((float)x / (float)fractionX) * plane.localScale.x * 10), 0, minZ + (((float)z / (float)fractionZ) * plane.localScale.z * 10)));
            }
        }
        return toReturn;
    }


    static Texture2D GenerateTexture(float scaleX, float scaleY, int widthT, int heightT)
    {
        float offsetX = Random.Range(0, 999);
        float offsetY = Random.Range(0, 999);
        Texture2D texture = new Texture2D(widthT, heightT);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color colour = CalculateColour(x, y, scaleX, scaleY, offsetY, offsetX);
                texture.SetPixel(x, y, colour);
            }
        }

        texture.Apply();
        return texture;
    }

    static Color CalculateColour(int x, int y,float scaleX, float scaleY, float oY, float oX)
    {
        float xCoord = (float)x / width * scaleX + oX;
        float yCoord = (float)y / height * scaleY + oY;

        float sample = Mathf.PerlinNoise(xCoord, yCoord);
        return new Color(sample, sample, sample);
    }

}
