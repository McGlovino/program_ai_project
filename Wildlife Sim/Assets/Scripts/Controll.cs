﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controll : MonoBehaviour
{
    public List<Rabbit> rabbits = new List<Rabbit>();
    public List<Fox> foxes = new List<Fox>();

    public GameObject selected = null;
    public MapGenerate MG;
    float timeCounter;

    public Text searchForfoodText, fleeText, stateText, speedText, FOVText, needToMateText, findingAMateText, hungerText
        , aveNeedToMateText, aveFOVText, aveSpeedText, foxAveNeedToMateText, foxAveFOVText, foxAveSpeedText, totalRabbitsText, totalFoxesText;
    public GameObject selectedStats;

    Camera thisCamera;

    void Start()
    {
        MG = GameObject.FindGameObjectWithTag("MapManager").GetComponent<MapGenerate>();
        thisCamera = this.GetComponent<Camera>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 800, LayerMask.GetMask("Floor")))
            {
                Collider[] inRadius = Physics.OverlapSphere(hit.transform.position, 1.8f, LayerMask.GetMask("Default"));
                foreach(Collider c in inRadius)
                {
                    if (c.gameObject.name == "Rabbit(Clone)")
                    {
                        if (selected != false)
                        {
                            if (FindRabbit(selected))
                                FindRabbit(selected).selected = false;
                            if (FindFox(selected))
                                FindFox(selected).selected = false;
                        }
                        selected = c.gameObject;
                        FindRabbit(selected).selected = true;
                        break;
                    }
                    else if (c.gameObject.name == "Fox(Clone)")
                    {
                        if (selected != false)
                        {
                            if(FindRabbit(selected))
                                FindRabbit(selected).selected = false;
                            if(FindFox(selected))
                                FindFox(selected).selected = false;
                        }
                        selected = c.gameObject;
                        FindFox(selected).selected = true;
                        break;
                    }
                }
            }
        }
        if (Input.GetKey(KeyCode.W))
            transform.position += new Vector3(0,0, Constants.cameraSpeed * thisCamera.fieldOfView/25);
        else if (Input.GetKey(KeyCode.S))
            transform.position -= new Vector3(0, 0, Constants.cameraSpeed * thisCamera.fieldOfView / 25);
        if (Input.GetKey(KeyCode.A))
            transform.position -= new Vector3(Constants.cameraSpeed * thisCamera.fieldOfView / 25, 0, 0);
        else if (Input.GetKey(KeyCode.D))
            transform.position += new Vector3(Constants.cameraSpeed * thisCamera.fieldOfView / 25, 0, 0);

        float fov = thisCamera.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * 15;
        fov = Mathf.Clamp(fov, 15, 90);
        thisCamera.fieldOfView = fov;

        if (selected == null)
            selectedStats.SetActive(false);
        else
        {
            selectedStats.SetActive(true);
            if (selected.GetComponent<Rabbit>() != null)
            {
                hungerText.text = "Hunger: " + (int)selected.GetComponent<Rabbit>().needs["hunger"];
                searchForfoodText.text = "SearchforFood: " + (int)selected.GetComponent<Rabbit>().desires["SearchForFood"];
                findingAMateText.text = "FindingAMate: " + (int)selected.GetComponent<Rabbit>().desires["FindingAMate"];
                fleeText.text = "Flee: " + (int)selected.GetComponent<Rabbit>().desires["Flee"];
                stateText.text = "State: " + selected.GetComponent<Rabbit>().state.stateName;
                speedText.text = "Speed: " + selected.GetComponent<Rabbit>().attributes[0];
                FOVText.text = "FOV: " + selected.GetComponent<Rabbit>().attributes[1];
                needToMateText.text = "Mating Need: " + selected.GetComponent<Rabbit>().attributes[2];
            }
            else
            {
                hungerText.text = "Hunger: " + (int)selected.GetComponent<Fox>().needs["hunger"];
                searchForfoodText.text = "SearchforFood: " + (int)selected.GetComponent<Fox>().desires["SearchForFood"];
                findingAMateText.text = "FindingAMate: " + (int)selected.GetComponent<Fox>().desires["FindingAMate"];
                fleeText.text = "";
                stateText.text = "State: " + selected.GetComponent<Fox>().state.stateName;
                speedText.text = "Speed: " + selected.GetComponent<Fox>().attributes[0];
                FOVText.text = "FOV: " + selected.GetComponent<Fox>().attributes[1];
                needToMateText.text = "Mating Need: " + selected.GetComponent<Fox>().attributes[2];
            }

            if (Input.GetKey(KeyCode.Space)){
                Vector3 centerPoint = thisCamera.ScreenToWorldPoint(new Vector3(thisCamera.pixelWidth/2, thisCamera.pixelHeight/2, thisCamera.nearClipPlane));
                Vector3 difference = selected.transform.position - centerPoint;
                difference.y = 0;
                difference.z -= 13;
                transform.position = Vector3.Lerp(transform.position, transform.position + difference, 0.3f);
            }
        }
        totalRabbitsText.text = "Total Rabbits: " + rabbits.Count;
        totalFoxesText.text = "Total Foxes: " + foxes.Count;
        //Rabbits average
        float accumulation = 0;
        foreach (Rabbit r in rabbits)
            accumulation += r.attributes[0];
        float average = accumulation / rabbits.Count;
        aveSpeedText.text = "Speed: " + average;
        accumulation = 0;
        foreach (Rabbit r in rabbits)
            accumulation += r.attributes[1];
        average = accumulation / rabbits.Count;
        aveFOVText.text = "FOV: " + average;
        accumulation = 0;
        foreach (Rabbit r in rabbits)
            accumulation += r.attributes[2];
        average = accumulation / rabbits.Count;
        aveNeedToMateText.text = "Mating Need: " + average;
        //foxes average
        accumulation = 0;
        foreach (Fox f in foxes)
            accumulation += f.attributes[0];
        average = accumulation / foxes.Count;
        foxAveSpeedText.text = "Speed: " + average;
        accumulation = 0;
        foreach (Fox f in foxes)
            accumulation += f.attributes[1];
        average = accumulation / foxes.Count;
        foxAveFOVText.text = "FOV: " + average;
        accumulation = 0;
        foreach (Fox f in foxes)
            accumulation += f.attributes[2];
        average = accumulation / foxes.Count;
        foxAveNeedToMateText.text = "Mating Need: " + average;
    }
    public Rabbit FindRabbit(GameObject compare)
    {
        for(int i = 0; i < rabbits.Count; i ++)
        {
            if (rabbits[i].gObject == compare)
                return rabbits[i];
        }
        return null;
    }

    public Fox FindFox(GameObject compare)
    {
        for (int i = 0; i < foxes.Count; i++)
        {
            if (foxes[i].gObject == compare)
                return foxes[i];
        }
        return null;
    }
}
