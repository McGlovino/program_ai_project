﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour
{
    Controll controll;
    public Graph graph;

    public GameObject gObject;
    public MeshRenderer stateCube;
    public Material[] stateColours;

    public MatingDesirability mDesire;
    public SearchForFoodDesirabilityFox sDesire;

    public Dictionary<string, float> needs;
    public Dictionary<string, float> desires;
    public List<int> attributes = new List<int>();
    float newNeedToMate;
    public int movesToMate;

    public Node currentNode;
    public int currentWP = 0;

    float counter;

    public bool selected = false;

    public List<CanSee> canSee;
    public CanSee closestFox = new CanSee();
    public CanSee closestRabbit = new CanSee();
    bool bred = false;
    bool isChild = false;
    float childTime;
    public State<Fox> state;
    public struct CanSee
    {
        public GameObject gObject;
        public float distance;
        public GameObject node;
        public CanSee(GameObject gObjectT, float distanceT, GameObject nodeT)
        {
            gObject = gObjectT;
            distance = distanceT;
            node = nodeT;
        }
    }

    private void attributePoints(int attribute)
    {
        if (attributes[attribute] < Constants.attributeLimit)
            attributes[attribute]++;
        else
            attributePoints(Random.Range(0, attributes.Count));
    }
    public void attributePoints(Fox r1, Fox r2)
    {
        isChild = true;
        transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);

        attributes = new List<int>() {1 //speed
            , 1  // fov
            , 1 }; // need to mate

        int totalPoints = 0;
        for (int i = 0; i < attributes.Count; i++)
        {
            attributes[i] += ((int)(r1.attributes[i] + r2.attributes[i]) / 2) + Random.Range(-1, 2); //average with deviation
            if (attributes[i] < 1)
                attributes[i] = 1;
            if (attributes[i] > Constants.attributeLimit)
                attributes[i] = Constants.attributeLimit;
            totalPoints += (attributes[i]) - 1;
        }
        //points assigned or taken away from attributes to keep total points the same accross generations
        int difference = totalPoints - (attributes.Count * Constants.foxAveragePoints);
        for (int i = 0; i < Mathf.Abs(difference); i++)
        {
            int randomFox = Random.Range(0, attributes.Count);
            if (difference > 0)
            {
                while (attributes[randomFox] == 0)
                    randomFox = Random.Range(0, attributes.Count);
                attributes[randomFox]--;
            }
            else
            {
                while (attributes[randomFox] > Constants.attributeLimit)
                    randomFox = Random.Range(0, attributes.Count);
                attributes[randomFox]++;
            }
        }
        newNeedToMate = (((attributes[2] - 0) * (Constants.foxMaxNeedToMatePS - 0.5f)) / (Constants.attributeLimit - 0)) + 0;
        //NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
    }
    void Start()
    {
        if (!bred)
        {
            attributes = new List<int>() {1 //speed
            , 1  // fov
            , 1 }; // need to mate

            int pointsToAssign = attributes.Count * Constants.foxAveragePoints;
            while (pointsToAssign > 0)
            {
                attributePoints(Random.Range(0, attributes.Count));
                pointsToAssign--;
            }
            newNeedToMate = (((attributes[2] - 0) * (Constants.foxMaxNeedToMatePS - 0.5f)) / (Constants.attributeLimit - 0)) + 0;
        }

        needs = new Dictionary<string, float>
        {
            {"hunger", 0},
            {"mating", 0}
        };

        desires = new Dictionary<string, float>
        {
            {"Wander", Constants.wanderDesire},
            {"SearchForFood", 0},
            {"FindingAMate", 0}
        };

        canSee = new List<CanSee>();
        state = new WanderFox();
        graph = GameObject.FindGameObjectWithTag("MapManager").GetComponent<MapGenerate>().graph.Deepcopy();
        controll = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Controll>();
        controll.foxes.Add(this);
        currentNode = graph.FindNode(Functions.isGrounded2(this.gObject, LayerMask.GetMask("Floor")));
        Look();
    }

    void Update()
    {
        Look();
        childTime += Time.deltaTime;
        if (childTime >= Constants.foxTimeAsChild)
            BecomeAdult();
        UpdateNeeds();
        UpdateDesires();
        DecideOnState(false);
        state.Execute(this);
    }

    private void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentWP == graph.getPathLength())
        {
            //Look();
            graph.pathList.Clear();
            return;
        }

        currentNode = graph.FindNode(Functions.isGrounded2(gObject, LayerMask.GetMask("Floor")));

        if (gObject.transform.position.x == graph.getPathPoint(currentWP).transform.position.x
            && gObject.transform.position.z == graph.getPathPoint(currentWP).transform.position.z)
            currentWP++;

        if (currentWP < graph.getPathLength())
            MoveTo(graph.getPathPoint(currentWP).transform.position);

        if (selected)
        {
            Collider[] floor = Physics.OverlapSphere(currentNode.gObject.transform.position, attributes[1] + Constants.foxAddFOV, LayerMask.GetMask("Floor"));
            foreach (Collider c in floor)
                c.gameObject.GetComponent<Deactivate>().active = true;
        }

    }

    private void BecomeAdult()
    {
        isChild = false;
        transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
    }

    private void UpdateNeeds()
    {
        needs["hunger"] += Time.deltaTime;
        //kill the rabit
        if (needs["hunger"] >= Constants.foxStarve)
        {
            if (selected)
                controll.selected = null;
            controll.foxes.Remove(this);
            Destroy(this.gObject);
            Destroy(this);
        }

        if (!isChild)
        {
            needs["mating"] += Time.deltaTime * newNeedToMate;
            if (needs["mating"] > 100)
                needs["mating"] = 100;
        }
        mDesire.matingNeed = needs["mating"];
        sDesire.hunger = needs["hunger"];
    }

    private void UpdateDesires()
    {
        // SearchForFood:
        // hunger, distance to closest Rabbit
        // FindingAMate:
        // mating need, distance to closest mating Fox

        desires["FindingAMate"] = mDesire.crispDesirability;
        //Debug.Log(sDesire.crispDesirability);
        desires["SearchForFood"] = sDesire.crispDesirability;
    }

    public void MoveTo(Vector3 pos)
    {
        Vector3 startPos = gObject.transform.position;

        Vector3 direction = new Vector3(pos.x, gObject.transform.position.y, pos.z) - gObject.transform.position;
        if ((direction.normalized * Time.deltaTime * ((attributes[0] + Constants.foxAddSpeed) *  currentNode.canWalk)).magnitude > direction.magnitude)
            gObject.transform.position += direction;
        else
            gObject.transform.position += direction.normalized * Time.deltaTime * ((attributes[0] + Constants.foxAddSpeed) *currentNode.canWalk);
        if (startPos == gObject.transform.position)
            gObject.transform.position = pos;

        gObject.transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(gObject.transform.forward, direction, 0.4f, 0.0f));
    }

    public void Look()
    {
        if (canSee != null)
            canSee.Clear();
        Collider[] inRadius = Physics.OverlapSphere(currentNode.gObject.transform.position, attributes[1] + Constants.foxAddFOV, LayerMask.GetMask("Default"));
        foreach (Collider c in inRadius)
        {
            if (c.gameObject.tag == "Object" && c.gameObject != gObject)
                canSee.Add(new CanSee(c.gameObject, (c.gameObject.transform.position - gObject.transform.position).magnitude, Functions.isGrounded2(c.gameObject, LayerMask.GetMask("Floor"))));
        }
        FindClosest();
    }

    public void FindClosest()
    {
        ResetClosest();
        foreach (CanSee c in canSee)
        {
            if (c.gObject != null && c.gObject.name == "Fox(Clone)"
                && controll.FindFox(c.gObject) != null
                && controll.FindFox(c.gObject).state.stateName == "FindingAMate"
                && controll.FindFox(c.gObject).currentNode != currentNode)
            {
                if (c.node == null)
                    continue;
                if (c.distance < closestFox.distance)
                    closestFox = c;
            }
            else if (c.gObject != null && c.gObject.name == "Rabbit(Clone)")
            {
                if (c.node == null)
                    continue;
                if (c.distance < closestRabbit.distance)
                    closestRabbit = c;
            }
        }
        //caps at 20
        sDesire.closestFood = (closestRabbit.distance > 20 ? 20 : closestRabbit.distance);
        if (!isChild)
            mDesire.closestMate = (closestFox.distance > 20 ? 20 : closestFox.distance);
        else
            mDesire.closestMate = 20;
    }
    void ResetClosest()
    {
        closestRabbit = new CanSee();
        closestFox = new CanSee();
        closestRabbit.distance = 99999;
        closestFox.distance = 99999;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Rabbit(Clone)" && state.stateName == "SearchForFood")
        {
            needs["hunger"] -= Constants.rabbitFoodWorth;
            if (needs["hunger"] < 0)
                needs["hunger"] = 0;
            sDesire.hunger = needs["hunger"];
            controll.rabbits.Remove(controll.FindRabbit(other.gameObject));
            Destroy(other.gameObject);
            DecideOnState(true);
        }
        if (other.gameObject.name == "Fox(Clone)")
        {
            Fox otherFox = controll.FindFox(other.gameObject);
            if (otherFox != null)
            {
                if (otherFox.state.stateName == "FindingAMate")
                {
                    movesToMate--;
                }
                if (movesToMate <= 0)
                {
                    if (selected)
                        Debug.Log("Hit");
                    needs["mating"] -= 9999;
                    mDesire.matingNeed = 0;
                    otherFox.needs["mating"] -= 9999;
                    otherFox.mDesire.matingNeed = 0;
                    movesToMate = 3;
                    otherFox.movesToMate = 3;
                    MakeBaby(otherFox);
                    mDesire.closestMate = 20;
                    otherFox.mDesire.closestMate = 20;
                    DecideOnState(true);
                    otherFox.DecideOnState(true);
                }
                if (needs["mating"] < 0)
                    needs["mating"] = 0;
                if (otherFox.needs["mating"] < 0)
                    otherFox.needs["mating"] = 0;
            }
        }
    }

    private void MakeBaby(Fox otherFox)
    {
        while (true)
        {
            for (int i = 0; i < 8; i++)
            {
                Node temp = graph.FindNode(currentNode.direction(i));
                if (temp != null && Functions.whatsOn(temp.gObject.transform.position, LayerMask.GetMask("Default")) == null && temp.canWalk > 0)
                {
                    GameObject baby = Instantiate(Resources.Load("Prefabs/Fox") as GameObject, temp.gObject.transform.position, transform.rotation);
                    baby.GetComponent<Fox>().bred = true;
                    baby.GetComponent<Fox>().attributePoints(this, otherFox);
                    graph.pathList.Clear();
                    currentWP = 0;
                    return;
                }

            }
        }
    }

    public void ChangeState(State<Fox> newState)
    {
        state = newState;
    }

    public string DecideOnState(bool overridePathLength)
    {
        if ((graph.getPathLength() == 0
            || (currentWP > 1
            && transform.position == currentNode.gObject.transform.position))
            || overridePathLength)
        {
            KeyValuePair<string, float> highestDesire = new KeyValuePair<string, float>();

            foreach (KeyValuePair<string, float> k in desires)
            {
                if (k.Value > highestDesire.Value)
                    highestDesire = k;
            }
            switch (highestDesire.Key)
            {
                case "Wander":
                    stateCube.material = stateColours[0];
                    ChangeState(new WanderFox());
                    break;
                case "SearchForFood":
                    stateCube.material = stateColours[1];
                    ChangeState(new SearchForFoodFox());
                    break;
                case "FindingAMate":
                    stateCube.material = stateColours[2];
                    ChangeState(new FindingAMateFox());
                    break;
                default:
                    break;
            }
            return highestDesire.Key;
        }
        return null;
    }
}