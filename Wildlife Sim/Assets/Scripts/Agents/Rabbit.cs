﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rabbit : MonoBehaviour
{
    Controll controll;
    public Graph graph;

    public GameObject gObject;
    public MeshRenderer stateCube;
    public Material[] stateColours;

    public List<CanSee> canSee;
    public CanSee closestRabbit = new CanSee();
    public CanSee closestGrass = new CanSee();
    public CanSee closestFox = new CanSee();
    public int visibleFoxes = 0;

    public MatingDesirability mDesire;
    public SearchForFoodDesirability sDesire;
    public FleeDesirability fDesire;

    public Dictionary<string, float> needs;
    public Dictionary<string, float> desires;
    public List<int> attributes = new List<int>();
    float newNeedToMate;
    public int movesToMate;

    public Node currentNode;
    public int currentWP = 0;

    float counter;

    public bool selected = false;

    bool bred = false;
    bool isChild = false;
    float childTime;
    public State<Rabbit> state;
    public struct CanSee
    {
        public GameObject gObject;
        public float distance;
        public GameObject node;
        public CanSee(GameObject gObjectT, float distanceT, GameObject nodeT)
        {
            gObject = gObjectT;
            distance = distanceT;
            node = nodeT;
        }
    }

    private void attributePoints(int attribute)
    {
        if (attributes[attribute] < Constants.attributeLimit)
            attributes[attribute]++;
        else
            attributePoints(Random.Range(0, attributes.Count));
    }
    public void attributePoints(Rabbit r1, Rabbit r2)
    {
        isChild = true;
        transform.localScale = new Vector3(0.45f, 0.45f, 0.45f);

        attributes = new List<int>() {1 //speed
            , 1  // fov
            , 1 }; // need to mate

        int totalPoints = 0;
        for(int i = 0; i < attributes.Count; i++)
        {
            attributes[i] += ((int)(r1.attributes[i] + r2.attributes[i]) / 2) + Random.Range(-1, 2); //average with deviation
            if (attributes[i] < 1)
                attributes[i] = 1;
            if (attributes[i] > Constants.attributeLimit)
                attributes[i] = Constants.attributeLimit;
            totalPoints += (attributes[i]) -1;
        }
        //points assigned or taken away from attributes to keep total points the same accross generations
        int difference = totalPoints - (attributes.Count * Constants.rabbitAveragePoints);
        for(int i = 0; i < Mathf.Abs(difference); i++)
        {
            int randomRabbit = Random.Range(0, attributes.Count);
            if (difference > 0)
            {
                while (attributes[randomRabbit] == 0)
                    randomRabbit = Random.Range(0, attributes.Count);
                attributes[randomRabbit]--;
            }
            else
            {
                while (attributes[randomRabbit] > Constants.attributeLimit)
                    randomRabbit = Random.Range(0, attributes.Count);
                attributes[randomRabbit]++;
            }
        }
        newNeedToMate = (((attributes[2] - 0) * (Constants.rabbitMaxNeedToMatePS - 0.5f)) / (Constants.attributeLimit - 0)) + 0;
        //NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
    }
    void Start()
    {
        if (!bred)
        {
            attributes = new List<int>() {1 //speed
            , 1  // fov
            , 1 }; // need to mate

            int pointsToAssign = attributes.Count * Constants.rabbitAveragePoints;
            while (pointsToAssign > 0)
            {
                attributePoints(Random.Range(0, attributes.Count));
                pointsToAssign--;
            }
            newNeedToMate = (((attributes[2] - 0) * (Constants.rabbitMaxNeedToMatePS - 0.5f)) / (Constants.attributeLimit - 0)) + 0;
        }

        needs = new Dictionary<string, float>
        {
            {"hunger", 0},
            {"mating", 0}
        };

        desires = new Dictionary<string, float>
        {
            {"Wander", Constants.wanderDesire},
            {"SearchForFood", 0},
            {"Flee", 0},
            {"FindingAMate", 0}
        };

        canSee = new List<CanSee>();
        state = new Wander();
        graph = GameObject.FindGameObjectWithTag("MapManager").GetComponent<MapGenerate>().graph.Deepcopy();
        controll = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Controll>();
        controll.rabbits.Add(this);
        currentNode = graph.FindNode(Functions.isGrounded2(this.gObject, LayerMask.GetMask("Floor")));
        Look();
    }

    void Update()
    {
        Look();
        childTime += Time.deltaTime;
        if (childTime >= Constants.rabbitTimeAsChild)
            BecomeAdult();
        UpdateNeeds();
        UpdateDesires();
        DecideOnState(false);
        state.Execute(this);
    }

    private void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentWP == graph.getPathLength())
        {
            //Look();
            graph.pathList.Clear();
            return;
        }

        currentNode = graph.FindNode(Functions.isGrounded2(gObject, LayerMask.GetMask("Floor")));

        if (gObject.transform.position.x == graph.getPathPoint(currentWP).transform.position.x 
            && gObject.transform.position.z == graph.getPathPoint(currentWP).transform.position.z)
            currentWP++;

        if (currentWP < graph.getPathLength())
            MoveTo(graph.getPathPoint(currentWP).transform.position);

        if (selected)
        {
            Collider[] floor = Physics.OverlapSphere(currentNode.gObject.transform.position, attributes[1] + Constants.rabbitAddFOV, LayerMask.GetMask("Floor"));
            foreach (Collider c in floor)
                c.gameObject.GetComponent<Deactivate>().active = true;
        }

    }

    private void BecomeAdult()
    {
        isChild = false;
        transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
    }

    private void UpdateNeeds()
    {
        needs["hunger"] += Time.deltaTime;
        //kill the rabit
        if (needs["hunger"] >= Constants.rabbitStarve)
        {
            if (selected)
                controll.selected = null;
            controll.rabbits.Remove(this);
            Destroy(this.gObject);
            Destroy(this);
        }

        if (!isChild)
        {
            needs["mating"] += Time.deltaTime * newNeedToMate;
            if (needs["mating"] > 100)
                needs["mating"] = 100;
        }
        mDesire.matingNeed = needs["mating"];
        sDesire.hunger = needs["hunger"];
    }

    private void UpdateDesires()
    {
        // SearchForFood:
        // hunger, distance to closest grass
        // FindingAMate:
        // mating need, distance to closest mating rabbit

        desires["FindingAMate"] = mDesire.crispDesirability;
        //Debug.Log(sDesire.crispDesirability);
        desires["SearchForFood"] = sDesire.crispDesirability;
        desires["Flee"] = fDesire.crispDesirability;
    }

    public void MoveTo(Vector3 pos)
    {
        Vector3 startPos = gObject.transform.position;

        Vector3 direction = new Vector3(pos.x, gObject.transform.position.y, pos.z) - gObject.transform.position;
        if ((direction.normalized * Time.deltaTime * ((attributes[0] + Constants.rabbitAddSpeed) * currentNode.canWalk)).magnitude > direction.magnitude)
            gObject.transform.position += direction;
        else
            gObject.transform.position += direction.normalized * Time.deltaTime * ((attributes[0] + Constants.rabbitAddSpeed) * currentNode.canWalk);
        if (startPos == gObject.transform.position)
            gObject.transform.position = pos;

        gObject.transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(gObject.transform.forward, direction, 0.4f, 0.0f));
    }

    public void Look()
    {
        if (canSee != null)
            canSee.Clear();

        Collider[] inRadius = Physics.OverlapSphere(currentNode.gObject.transform.position, attributes[1] + Constants.rabbitAddFOV, LayerMask.GetMask("Default"));
        foreach (Collider c in inRadius)
        {
            if (c.gameObject.tag == "Object" && c.gameObject != gObject)
                canSee.Add(new CanSee(c.gameObject, (c.gameObject.transform.position - gObject.transform.position).magnitude, Functions.isGrounded2(c.gameObject, LayerMask.GetMask("Floor"))));
        }
        FindClosest();
    }

    public void FindClosest()
    {
        ResetClosest();

        foreach (CanSee c in canSee)
        {
            if (c.gObject != null && c.gObject.name == "Rabbit(Clone)" 
                && controll.FindRabbit(c.gObject) != null 
                && controll.FindRabbit(c.gObject).state.stateName == "FindingAMate"
                && controll.FindRabbit(c.gObject).currentNode != currentNode)
            {
                if (c.node == null)
                    continue;
                if (c.distance < closestRabbit.distance)
                    closestRabbit = c;
            }
            else if (c.gObject != null && c.gObject.name == "Grass 2")
            {
                if (c.node == null)
                    continue;
                if (c.distance < closestGrass.distance)
                    closestGrass = c;
            }
            else if (c.gObject != null && c.gObject.name == "Fox(Clone)")
            {
                if (c.node == null)
                    continue;
                visibleFoxes ++;
                if (c.distance < closestFox.distance)
                    closestFox = c;
            }
        }
        //Set the desires closest items
        //Caps at 20 safety
        sDesire.closestFood = (closestGrass.distance > 20 ? 20 : closestGrass.distance);
        if (!isChild)
            mDesire.closestMate = (closestRabbit.distance > 20 ? 20 : closestRabbit.distance);
        else
            mDesire.closestMate = 20;
        fDesire.closestFox = (closestFox.distance > 20 ? 20 : closestFox.distance);
        fDesire.foxesInRadius = visibleFoxes;
    }

    void ResetClosest()
    {
        closestRabbit = new CanSee();
        closestGrass = new CanSee();
        closestFox = new CanSee();
        visibleFoxes = 0;
        closestRabbit.distance = 99999;
        closestGrass.distance = 99999;
        closestFox.distance = 99999;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Grass 2")
        {
            needs["hunger"] -= Constants.grassFoodWorth;
            if (needs["hunger"] < 0)
                needs["hunger"] = 0;
            sDesire.hunger = needs["hunger"];
            graph.FindNode(Functions.isGrounded2(other.gameObject, LayerMask.GetMask("Floor"))).objectOn = false;
            Destroy(other.gameObject);
            DecideOnState(true);
        }
        if (other.gameObject.name == "Rabbit(Clone)")
        {
            Rabbit otherRabbit = controll.FindRabbit(other.gameObject);
            if (otherRabbit != null)
            {
                if (otherRabbit.state.stateName == "FindingAMate")
                    movesToMate--;
                if (movesToMate <= 0)
                {
                    needs["mating"] -= 9999;
                    mDesire.matingNeed = 0;
                    movesToMate = 3;
                    MakeBaby(otherRabbit);
                    if (Random.Range(0, 4) == 1) // chance of twins
                        MakeBaby(otherRabbit);
                    if (Random.Range(0, 4) == 1) // chance of triplets
                        MakeBaby(otherRabbit);
                    DecideOnState(true);
                    otherRabbit.needs["mating"] -= 9999;
                    otherRabbit.mDesire.matingNeed = 0;
                    otherRabbit.movesToMate = 3;
                    otherRabbit.DecideOnState(true);
                }
                if (needs["mating"] < 0)
                    needs["mating"] = 0;
                if (otherRabbit.needs["mating"] < 0)
                    otherRabbit.needs["mating"] = 0;
            }
        }
    }

    private void MakeBaby(Rabbit otherRabbit)
    {
        while (true)
        {
            for (int i = 0; i < 8; i++)
            {
                Node temp = graph.FindNode(currentNode.direction(i));
                if (temp != null && Functions.whatsOn(temp.gObject.transform.position, LayerMask.GetMask("Default")) == null && temp.canWalk > 0)
                {
                    GameObject baby = Instantiate(Resources.Load("Prefabs/Rabbit") as GameObject, temp.gObject.transform.position, transform.rotation);
                    baby.GetComponent<Rabbit>().bred = true;
                    baby.GetComponent<Rabbit>().attributePoints(this, otherRabbit);
                    //Debug.Log("Baby Made");
                    graph.pathList.Clear();
                    currentWP = 0;
                    return;
                }

            }
        }
    }

    public void ChangeState(State<Rabbit> newState)
    {
        state = newState;
    }

    public string DecideOnState(bool overridePathLength)
    {
        if ((graph.getPathLength() == 0
            || (currentWP > 1
            && transform.position == currentNode.gObject.transform.position))
            || overridePathLength)
        {
            KeyValuePair<string, float> highestDesire = new KeyValuePair<string, float>();

            foreach (KeyValuePair<string, float> k in desires)
            {
                if (k.Value > highestDesire.Value)
                    highestDesire = k;
            }
            switch (highestDesire.Key)
            {
                case "Wander":
                    stateCube.material = stateColours[0];
                    ChangeState(new Wander());
                    break;
                case "SearchForFood":
                    stateCube.material = stateColours[1];
                    ChangeState(new SearchForFood());
                    break;
                case "Flee":
                    stateCube.material = stateColours[3];
                    ChangeState(new Flee());
                    break;
                case "FindingAMate":
                    stateCube.material = stateColours[2];
                    ChangeState(new FindingAMate());
                    break;
                default:
                    break;
            }
            return highestDesire.Key;
        }
        return null;
    }
}
