﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Rabbit;

public class State<T>
{
    public string stateName = "Base";
    public virtual void Execute(T pass){}
    public bool lookingElsewhere = false;
    public Controll controll = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Controll>();
    public void lookElsewhere(Rabbit pass)
    {
        lookingElsewhere = true;
        if (pass.graph.getPathLength() == 0)
        {
            GameObject to = pass.graph.randomInRadius(pass.gObject.transform.position, pass.attributes[1] + Constants.rabbitAddFOV * 2);
            if (to != pass.currentNode.gObject && pass.graph.AStar(pass.currentNode.gObject, to))
                pass.currentWP = 0;
            else
                lookElsewhere(pass);
        }
    }
    public void lookElsewhere(Fox pass)
    {
        lookingElsewhere = true;
        if (pass.graph.getPathLength() == 0)
        {
            GameObject to = pass.graph.randomInRadius(pass.gObject.transform.position, pass.attributes[1] + Constants.foxAddFOV * 2);
            if (to != pass.currentNode.gObject && pass.graph.AStar(pass.currentNode.gObject, to))
                pass.currentWP = 0;
            else
                lookElsewhere(pass);
        }
    }
}

public class SearchForFood : State<Rabbit>
{
    public SearchForFood()
    {
        stateName = "SearchForFood";
    }
    //public bool lookingElsewhere = false;
    public override void Execute(Rabbit pass)
    {
        if ((pass.graph.getPathLength() == 0
            || (lookingElsewhere && pass.closestGrass.gObject != null)
            && pass.graph.getPathLength() > 0)
            && pass.DecideOnState(true) == "SearchForFood")
        {
            lookingElsewhere = false;
            if (pass.canSee.Count == 0)
                lookElsewhere(pass);
            if (pass.closestGrass.gObject != null
                && pass.closestGrass.node != null 
                && pass.closestGrass.node != pass.currentNode.gObject)
            {
                if (pass.graph.AStar(pass.currentNode.gObject, pass.closestGrass.node))
                    pass.currentWP = 0;
                else if (pass.canSee.Count > 1)
                {
                    pass.canSee.Remove(pass.closestGrass);
                    pass.FindClosest();
                    Execute(pass);
                }
                else
                    lookElsewhere(pass);
            }
            else
                lookElsewhere(pass);
        }
    }
}

public class Wander : State<Rabbit>
{
    public Wander()
    {
        stateName = "Wander";
    }
    public override void Execute(Rabbit pass)
    {
        if (pass.graph.getPathLength() == 0)
        {
            pass.DecideOnState(false);
            if (pass.state.stateName != "Wander" && pass.canSee.Count > 0)
            {
                pass.state.Execute(pass); // makes sure it still wants to wander once the path has finished, if not do the execute it wants
                return;
            }
            GameObject to = pass.graph.randomInRadius(pass.gObject.transform.position, pass.attributes[1]+ Constants.rabbitAddFOV * 2);
            if (to != pass.currentNode.gObject && pass.graph.AStar(pass.currentNode.gObject, to))
                pass.currentWP = 0;
            else
                Execute(pass);
        }
    }
}

public class Flee : State<Rabbit>
{
    public Flee()
    {
        stateName = "Flee";
    }
    public override void Execute(Rabbit pass)
    {
        //Updates every node unlike most other states, allows more dynamic flee
        if (pass.closestFox.gObject != null
            && controll.FindFox(pass.closestFox.gObject) != null
            && (pass.graph.getPathLength() == 0
            || (pass.currentWP > 1
            && pass.transform.position == pass.currentNode.gObject.transform.position))) // so it doesnt go every frame and look twitchy
        {
            Vector3 desiredDirection = (pass.transform.position - pass.closestFox.gObject.transform.position).normalized;
            GameObject nodeTo = findPathPoint(desiredDirection, pass, 0, false);
            if (nodeTo != null && pass.graph.FindNode(nodeTo) != pass.currentNode
                && pass.graph.AStar(pass.currentNode.gObject, nodeTo))
                pass.currentWP = 0;
            else
                lookElsewhere(pass);
        }
        else if (pass.graph.getPathLength() == 0)
            lookElsewhere(pass);
    }

    GameObject findPathPoint(Vector3 desired, Rabbit pass, int counter, bool negativeDirection)
    {
        Vector3 point;
        GameObject on;

        for(int i = 15; i >= 5; i--)
        {
            point = pass.transform.position + (desired * i);
            on = Functions.isGrounded2(point, LayerMask.GetMask("Floor"));
            if (on != null && pass.graph.FindNode(on) != null)
                return on;
        }
        if (counter > 15)
            return null;

        //If it cant go in desired direction then try other angles
        if((counter == 0 && Random.Range(0,1) == 1) || negativeDirection)
            return findPathPoint(Quaternion.AngleAxis(-10, Vector3.up) * desired, pass, counter + 1, true);
        else
            return findPathPoint(Quaternion.AngleAxis(10, Vector3.up) * desired, pass, counter + 1, false);
    }
}
public class FindingAMate : State <Rabbit>
{
    public FindingAMate()
    {
        stateName = "FindingAMate";
    }
    public override void Execute(Rabbit pass)
    {
        if (pass.graph.getPathLength() == 0
            || (lookingElsewhere && pass.canSee.Count > 0)
            && pass.graph.getPathLength() > 0 
            && pass.closestGrass.gObject != null)
        {
            lookingElsewhere = false;
            if (pass.canSee.Count == 0)
                lookElsewhere(pass);
            if (pass.closestRabbit.gObject != null
                && pass.closestRabbit.node != null 
                && pass.closestRabbit.node != pass.currentNode.gObject)
            {
                if (pass.graph.AStar(pass.currentNode.gObject, pass.closestRabbit.node))
                    pass.currentWP = 0;
                else if (pass.canSee.Count > 1)
                {
                    pass.canSee.Remove(pass.closestRabbit);
                    pass.FindClosest();
                    Execute(pass);
                }
                else
                    lookElsewhere(pass);
            }
            else
                lookElsewhere(pass);
        }
        else if (pass.closestRabbit.gObject != null
            && pass.currentWP > 1
            && controll.FindRabbit(pass.closestRabbit.gObject) != null
            && controll.FindRabbit(pass.closestRabbit.gObject).currentNode != pass.currentNode
            && (pass.transform.position == pass.currentNode.gObject.transform.position) // so it doesnt go every frame and look twitchy
            && controll.FindRabbit(pass.closestRabbit.gObject).state.stateName == "FindingAMate") {
            pass.graph.pathList.Clear();
            if (pass.graph.AStar(pass.currentNode.gObject, controll.FindRabbit(pass.closestRabbit.gObject).currentNode.gObject))
                pass.currentWP = 0;
            else
                lookElsewhere(pass);
        }
    }
}

public class SearchForFoodFox : State<Fox>
{
    public SearchForFoodFox()
    {
        stateName = "SearchForFood";
    }
    public override void Execute(Fox pass)
    {
        if (pass.closestRabbit.gObject != null
            && controll.FindRabbit(pass.closestRabbit.gObject) != null
            && (pass.graph.getPathLength() == 0
            || (pass.currentWP > 1
            && pass.transform.position == pass.currentNode.gObject.transform.position)))
        {
            //Vector3 direction = pass.closestRabbit.gObject.transform.position - pass.transform.position;
            GameObject nodeTo = findPathPoint(pass.closestRabbit.gObject.transform.position, pass.closestRabbit.gObject.transform.forward, pass, 0, false);
            if (nodeTo != null && pass.graph.FindNode(nodeTo) != pass.currentNode
                && pass.graph.AStar(pass.currentNode.gObject, nodeTo))
                pass.currentWP = 0;
            else
                lookElsewhere(pass);
        }
        else if(pass.graph.getPathLength() == 0)
            lookElsewhere(pass);
    }
    GameObject findPathPoint(Vector3 desiredPos, Vector3 desiredForward, Fox pass, int counter, bool negativeDirection)
    {
        Vector3 point;
        GameObject on;

        point = desiredPos + (desiredForward * Mathf.Clamp((pass.transform.position - desiredPos).magnitude - 1, 0, 5));
        on = Functions.isGrounded2(point, LayerMask.GetMask("Floor"));
        if (on != null && pass.graph.FindNode(on) != null && pass.graph.FindNode(on) != pass.currentNode)
        {
            Debug.DrawRay(on.transform.position, Vector3.up * 2);
            return on;
        }
        if(counter > 15)
            return null;

        //If it cant go in desired direction then try other angles
        if ((counter == 0 && Random.Range(0, 1) == 1) || negativeDirection)
            return findPathPoint(desiredPos, Quaternion.AngleAxis(-10, Vector3.up) * desiredForward, pass, counter + 1, true);
        else
            return findPathPoint(desiredPos, Quaternion.AngleAxis(10, Vector3.up) * desiredForward, pass, counter + 1, false);
    }

}

public class WanderFox : State<Fox>
{
    public WanderFox()
    {
        stateName = "Wander";
    }
    public override void Execute(Fox pass)
    {
        if (pass.graph.getPathLength() == 0)
        {
            pass.DecideOnState(false);
            if (pass.state.stateName != "Wander" && pass.canSee.Count > 0) {
                pass.state.Execute(pass); // makes sure it still wants to wander once the path has finished, if not do the execute it wants
                return;
            }
            GameObject to = pass.graph.randomInRadius(pass.gObject.transform.position, pass.attributes[1] + Constants.foxAddFOV * 2);
            if (to != pass.currentNode.gObject && pass.graph.AStar(pass.currentNode.gObject, to))
                pass.currentWP = 0;
            else
                Execute(pass);
        }
    }
}
public class FindingAMateFox : State<Fox>
{
    public FindingAMateFox()
    {
        stateName = "FindingAMate";
    }
    public override void Execute(Fox pass)
    {
        if (pass.graph.getPathLength() == 0
            || (lookingElsewhere && pass.canSee.Count > 0)
            && pass.graph.getPathLength() > 0
            && pass.closestFox.gObject != null)
        {
            lookingElsewhere = false;
            if (pass.canSee.Count == 0)
                lookElsewhere(pass);
            if (pass.closestFox.gObject != null
                && pass.closestFox.node != null
                && pass.closestFox.node != pass.currentNode.gObject)
            {
                if (pass.graph.AStar(pass.currentNode.gObject, pass.closestFox.node))
                    pass.currentWP = 0;
                else if (pass.canSee.Count > 1)
                {
                    pass.canSee.Remove(pass.closestFox);
                    pass.FindClosest();
                    Execute(pass);
                }
                else
                    lookElsewhere(pass);
            }
            else
                lookElsewhere(pass);
        }
        else if (pass.closestFox.gObject != null
            && pass.currentWP > 1
            && controll.FindFox(pass.closestFox.gObject) != null
            && controll.FindFox(pass.closestFox.gObject).currentNode != pass.currentNode
            && (pass.transform.position == pass.currentNode.gObject.transform.position) // so it doesnt go every frame and look twitchy
            && controll.FindFox(pass.closestFox.gObject).state.stateName == "FindingAMate")
        {
            pass.graph.pathList.Clear();
            if (pass.graph.AStar(pass.currentNode.gObject, controll.FindFox(pass.closestFox.gObject).currentNode.gObject))
                pass.currentWP = 0;
            else
                lookElsewhere(pass);
        }
    }
}
