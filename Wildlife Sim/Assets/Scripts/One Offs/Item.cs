﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public enum typeOf {pickup, drop, select, collect, nothing};

    public string oName;
    public GameObject gObject;
    public typeOf type;
    public Vector3 spawnPos = new Vector3();

    public Item(GameObject gObjectT, string oNameT, typeOf typeT)
    {
        gObject = gObjectT;
        oName = oNameT;
        type = typeT;
    }

    public Item(string oNameT, typeOf typeT, Vector3 spawnPosT)
    {
        oName = oNameT;
        type = typeT;
        spawnPos = spawnPosT;
    }

    public Item(string oNameT, typeOf typeT)
    {
        oName = oNameT;
        type = typeT;
    }
}
