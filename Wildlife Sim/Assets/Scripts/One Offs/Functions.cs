﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Functions
{
    //REPLACE WITH FUNCTION TEMPLATE
    public static bool isGrounded(Vector3 v)
    {
        //Debug.DrawRay(v + new Vector3(0, 0.1f, 0), Vector3.down* 0.1f);
        return (Physics.Raycast(v + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f));
    }
    public static bool isGrounded(Transform t)
    {
        //Debug.DrawRay(t.position + new Vector3(0, 0.1f, 0), Vector3.down* 0.1f);
        return (Physics.Raycast(t.position + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f));
    }
    public static bool isGrounded(GameObject g)
    {
        //Debug.DrawRay(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down * 0.1f);
        return (Physics.Raycast(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f));
    }
    public static bool isGrounded(Vector3 v, float length)
    {
        //Debug.DrawRay(v + new Vector3(0, 0.1f, 0), Vector3.down* length);
        return (Physics.Raycast(v + new Vector3(0, 0.1f, 0), Vector3.down, length));
    }
    public static bool isGrounded(Transform t, float length)
    {
        //Debug.DrawRay(t.transform.position + new Vector3(0, 0.1f, 0), Vector3.down* length);
        return (Physics.Raycast(t.position + new Vector3(0, 0.1f, 0), Vector3.down, length));
    }
    public static bool isGrounded(GameObject g, float length)
    {
        //Debug.DrawRay(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down* length);
        return (Physics.Raycast(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down, length));
    }

    public static GameObject isGrounded2(GameObject g)
    {
        RaycastHit hit;
        Physics.Raycast(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down, out hit, 0.2f);
        return hit.transform.gameObject;
    }
    public static GameObject isGrounded2(GameObject g, LayerMask LM)
    {
        //Debug.DrawRay(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down * 0.3f);
        RaycastHit hit;
        if(Physics.Raycast(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down, out hit, 0.6f, LM))
            return hit.transform.gameObject;
        return null;
    }
    public static GameObject isGrounded2(Vector3 v)
    {
        RaycastHit hit;
        if (Physics.Raycast(v + new Vector3(0, 0.1f, 0), Vector3.down, out hit, 0.2f))
            return hit.transform.gameObject;
        return null;
    }
    public static GameObject isGrounded2(Vector3 v, LayerMask LM)
    {
        RaycastHit hit;
        if (Physics.Raycast(v + new Vector3(0, 0.1f, 0), Vector3.down, out hit, 0.2f, LM))
            return hit.transform.gameObject;
        return null;
    }
    public static GameObject whatsOn(Vector3 v, LayerMask LM)
    {
        //Debug.DrawRay(v + new Vector3(0, -0.1f, 0), Vector3.up * 0.5f);
        RaycastHit hit;
        if(Physics.Raycast(v + new Vector3(0, -0.1f, 0), Vector3.up, out hit, 0.2f, LM))
            return hit.transform.gameObject;
        return null;
    }
}
