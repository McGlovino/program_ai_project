﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivate : MonoBehaviour
{
    public bool active = false;
    public GameObject selected;


    void Update()
    {
        if (active && selected.transform.localScale.x <= 0.7)
            selected.transform.localScale += new Vector3(0.15f, 0.15f, 0.15f);
        else if(!active && selected.transform.localScale.x >= 0.15)
            selected.transform.localScale -= new Vector3(0.15f, 0.15f, 0.15f);

        active = false;
    }
}
