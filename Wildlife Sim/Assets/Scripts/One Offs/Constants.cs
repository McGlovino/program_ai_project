﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    // Hunger amount they die at
    public static int foxStarve = 80; //80
    public static int rabbitStarve = 50; //50

    // Average points per attribute to add to a base of 1
    public static int foxAveragePoints = 6; //6
    public static int rabbitAveragePoints = 6; //6

    // Highest points an attribute can reach
    public static int attributeLimit = 10; // 10

    // Time spent as a child (cannot breed whilst a child)
    public static int foxTimeAsChild = 80; //80
    public static int rabbitTimeAsChild = 10; //15

    // How much their need to mate increases per second (maximum)
    public static float foxMaxNeedToMatePS = 1.5f; //1.5
    public static float rabbitMaxNeedToMatePS = 2f; //2

    // Any desires less than this wont be executed
    // 0-100
    public static float wanderDesire = 10; //10

    // Adds to the base FOV
    public static int foxAddFOV = 3; //3
    public static int rabbitAddFOV = 3; //3

    // Adds to the base speed
    public static int foxAddSpeed = 0; //0
    public static int rabbitAddSpeed = 0; //0

    public static int grassFoodWorth = 5; //5
    public static int rabbitFoodWorth = 15; //15



    // Spawn rate of the grass
    // Allows map to support more animals
    public static float grassSpawnRate = 0.08f; //0.1f

    //Speed the players camera moves at
    public static float cameraSpeed = 0.6f; //0.6f
}
