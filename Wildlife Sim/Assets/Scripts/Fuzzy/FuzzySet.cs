﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuzzySet
{

    public float DOM;
    public float RepresentativeValue;
    public List<float> DOMs;

    public FuzzySet(float RepresentativeValue)
    {
        DOMs = new List<float>();
        DOM = 0.0f;
        this.RepresentativeValue = RepresentativeValue;
    }
    public virtual float CalculateDOM(float val)
    {
        return 0.0f;
    }

    //If value is higher than the current DOM, then set that as the DOM
    public void OR_DOM(float val)
    {
        if (DOM < val)
            DOM = val;
    }
    void ClearDOM()
    {
        DOM = 0.0f;
    }

}

public class FuzzySet_Triangle : FuzzySet
{
    private float peakPoint, leftPoint, rightPoint;

    public FuzzySet_Triangle(float mid, float left, float right) : base(mid)
    {
        peakPoint = mid;
        leftPoint = left;
        rightPoint = right;
    }
    public override float CalculateDOM(float val)
    {
        float rv = 0.0f;
        //DOM if left of centre
        if (val < peakPoint && val >= leftPoint)
        {
            float ZeroedVal = peakPoint - val;
            float ZeroedPeak = peakPoint - leftPoint;
            if (ZeroedPeak == 0)
                return rv;
            rv = ZeroedVal / ZeroedPeak;
        }
        //DOM right of centre
        else if (val > peakPoint && val <= rightPoint)
        {
            float ZeroedVal = val - peakPoint;
            float ZeroedRight = rightPoint - peakPoint;
            if (ZeroedRight == 0)
                return rv;
            rv = ZeroedVal / ZeroedRight;
        }
        //DOM is peak
        else if (val == peakPoint)
            return 1;
        else
            return 0;
        return 1-rv;
    }
}
public class FuzzySet_LeftShoulder : FuzzySet
{
    private float peakPoint, leftPoint, rightPoint;

    public FuzzySet_LeftShoulder(float mid, float left, float right) : base(left)//mid - ((mid-left)/2) for center
    {
        peakPoint = mid;
        leftPoint = left;
        rightPoint = right;
    }
    public override float CalculateDOM(float val)
    {
        float rv = 0.0f;
        //DOM if left of centre
        if (val <= peakPoint && val >= leftPoint)
            return 1;
        //DOM right of centre
        else if (val > peakPoint && val <= rightPoint)
        {
            float ZeroedVal = val - peakPoint;
            float ZeroedRight = rightPoint - peakPoint;
            if (ZeroedRight == 0)
                return rv;
            rv = ZeroedVal / ZeroedRight;
        }
        else
            return 0;
        return 1- rv;
    }
}

public class FuzzySet_RightShoulder : FuzzySet
{
    private float peakPoint, leftPoint, rightPoint;

    public FuzzySet_RightShoulder(float mid, float left, float right) : base(right)//mid +((right-mid)/2)
    {
        peakPoint = mid;
        leftPoint = left;
        rightPoint = right;
    }

    public override float CalculateDOM(float val)
    {
        float rv = 0.0f;
        //DOM if left of centre
        if (val < peakPoint && val >= leftPoint)
        {
            float ZeroedVal = peakPoint - val;
            float ZeroedPeak = peakPoint - leftPoint;
            if (ZeroedPeak == 0)
                return rv;
            rv = ZeroedVal / ZeroedPeak;
        }
        //DOM right of centre
        else if (val >= peakPoint && val <= rightPoint)
            return 1;
        else
            return 0;
        return 1 - rv;
    }
}