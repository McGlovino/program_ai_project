﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeDesirability : MonoBehaviour
{
    FuzzyModule RLDesire = new FuzzyModule();

    //Crisp inputs
    public float closestFox;
    public int foxesInRadius;

    //crisp output
    public float crispDesirability = 0.0f;

    void Start()
    {
        //Our Fuzzy Variables
        FuzzyVariable desirability, distanceToClosestFox, foxes;
        //Desirability
        desirability = RLDesire.CreateFLV("Desirability");
        FuzzySet undesirable = desirability.AddLeftShoulderSet("undesirable", 0.0f, 25.0f, 50.0f);
        FuzzySet desirable = desirability.AddTriangularSet("desirable", 25.0f, 50.0f, 75.0f);
        FuzzySet veryDesirable = desirability.AddRightShoulderSet("veryDesirable", 50.0f, 75.0f, 100.0f);

        //Distance To Closest Fox
        distanceToClosestFox = RLDesire.CreateFLV("DistanceToClosestFox");
        FuzzySet targetClose = distanceToClosestFox.AddLeftShoulderSet("targetClose", 0, 3, 5);
        FuzzySet targetMedium = distanceToClosestFox.AddTriangularSet("targetMedium", 3, 5, 10);
        FuzzySet targetFar = distanceToClosestFox.AddRightShoulderSet("targetFar", 5, 10, 20);

        //Amount of foxes
        foxes = RLDesire.CreateFLV("Foxes");
        FuzzySet foxesLow = foxes.AddLeftShoulderSet("foxesLow", 0, 1, 3);
        FuzzySet foxesOk = foxes.AddTriangularSet("foxesOk", 1, 3, 5);
        FuzzySet foxesLots = foxes.AddRightShoulderSet("foxesLots", 3, 5, 15);

        //rules
        RLDesire.AddRule(new FzAND(targetFar, foxesLots), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetFar, foxesOk), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetFar, foxesLow), new FuzzyTerm(undesirable));
        RLDesire.AddRule(new FzAND(targetMedium, foxesLots), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetMedium, foxesOk), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetMedium, foxesLow), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetClose, foxesLots), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetClose, foxesOk), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetClose, foxesLow), new FuzzyTerm(veryDesirable));
    }

    void Update()
    {
        //Calculate desirability every frame
        CalculateDesirability();
    }

    void CalculateDesirability()
    {
        //Firstly, fuzzify our two antecedents
        RLDesire.Fuzzify("DistanceToClosestFox", closestFox);
        RLDesire.Fuzzify("Foxes", foxesInRadius);

        //this would create our confidence values by evaulating against the ruleset
        //and store them in the consequent FuzzyVariable (which should be Desirability)
        RLDesire.FuzzyInference();

        //Get our crisp output.
        crispDesirability = RLDesire.Defuzzify("Desirability", FuzzyModule.DefuzzifyType.Max_Av);
    }
}
