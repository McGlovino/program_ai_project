﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatingDesirability : MonoBehaviour
{
    FuzzyModule RLDesire = new FuzzyModule();

    //Crisp inputs
    public float closestMate;
    public float matingNeed;

    //crisp output
    public float crispDesirability = 0.0f;

    void Start()
    {
        //Our Fuzzy Variables
        FuzzyVariable desirability, distanceToTarget, hunger;
        //Desirability
        desirability = RLDesire.CreateFLV("Desirability");
        FuzzySet undesirable = desirability.AddLeftShoulderSet("undesirable", 0.0f, 25.0f, 50.0f);
        FuzzySet desirable = desirability.AddTriangularSet("desirable", 25.0f, 50.0f, 75.0f);
        FuzzySet veryDesirable = desirability.AddRightShoulderSet("veryDesirable", 50.0f, 75.0f, 100.0f);

        //DistanceToTarget
        distanceToTarget = RLDesire.CreateFLV("DistanceToTarget");
        FuzzySet targetClose = distanceToTarget.AddLeftShoulderSet("targetClose", 0, 4, 6);
        FuzzySet targetMedium = distanceToTarget.AddTriangularSet("targetMedium", 4, 6, 10);
        FuzzySet targetFar = distanceToTarget.AddRightShoulderSet("targetFar", 6, 10, 20);

        //MatingStatus
        hunger = RLDesire.CreateFLV("MatingNeed");
        FuzzySet matingLow = hunger.AddLeftShoulderSet("matingLow", 0, 25, 50);
        FuzzySet matingOk = hunger.AddTriangularSet("matingOk", 25, 50, 75);
        FuzzySet matingHigh = hunger.AddRightShoulderSet("matingHigh", 50, 75, 100);

        //rules
        RLDesire.AddRule(new FzAND(targetFar, matingHigh), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetFar, matingOk), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetFar, matingLow), new FuzzyTerm(undesirable));
        RLDesire.AddRule(new FzAND(targetMedium, matingHigh), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetMedium, matingOk), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetMedium, matingLow), new FuzzyTerm(undesirable));
        RLDesire.AddRule(new FzAND(targetClose, matingHigh), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetClose, matingOk), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetClose, matingLow), new FuzzyTerm(undesirable));
    }

    void Update()
    {
        //Calculate desirability every frame
        CalculateDesirability();
    }

    void CalculateDesirability()
    {
        //Firstly, fuzzify our two antecedents
        RLDesire.Fuzzify("DistanceToTarget", closestMate);
        RLDesire.Fuzzify("MatingNeed", matingNeed);

        //this would create our confidence values by evaulating against the ruleset
        //and store them in the consequent FuzzyVariable (which should be Desirability)
        RLDesire.FuzzyInference();

        //Get our crisp output.
        crispDesirability = RLDesire.Defuzzify("Desirability", FuzzyModule.DefuzzifyType.Max_Av);
    }
}
