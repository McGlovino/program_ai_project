﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchForFoodDesirability : MonoBehaviour
{
    FuzzyModule RLDesire = new FuzzyModule();

    //Crisp inputs
    public float closestFood;
    public float hunger;

    //crisp output
    public float crispDesirability = 0.0f;

    void Start()
    {
        //Our Fuzzy Variables
        FuzzyVariable desirability, distanceToTarget, hunger;
        //Desirability
        desirability = RLDesire.CreateFLV("Desirability");
        FuzzySet undesirable = desirability.AddLeftShoulderSet("undesirable", 0.0f, 25.0f, 50.0f);
        FuzzySet desirable = desirability.AddTriangularSet("desirable", 25.0f, 50.0f, 75.0f);
        FuzzySet veryDesirable = desirability.AddRightShoulderSet("veryDesirable", 50.0f, 75.0f, 100.0f);

        //DistanceToTarget
        distanceToTarget = RLDesire.CreateFLV("DistanceToTarget");
        FuzzySet targetClose = distanceToTarget.AddLeftShoulderSet("targetClose", 0, 3, 5);
        FuzzySet targetMedium = distanceToTarget.AddTriangularSet("targetMedium", 3, 5, 10);
        FuzzySet targetFar = distanceToTarget.AddRightShoulderSet("targetFar", 5, 10, 20);

        //HungerStatus
        hunger = RLDesire.CreateFLV("Hunger");
        FuzzySet hungerLow = hunger.AddLeftShoulderSet("hungerLow", 0, 3, 15);
        FuzzySet hungerOk = hunger.AddTriangularSet("hungerOk", 3, 15, 30);
        FuzzySet hungerHigh = hunger.AddRightShoulderSet("hungerHigh", 15, 30, 50);

        //rules
        RLDesire.AddRule(new FzAND(targetFar, hungerHigh), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetFar, hungerOk), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetFar, hungerLow), new FuzzyTerm(undesirable));
        RLDesire.AddRule(new FzAND(targetMedium, hungerHigh), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetMedium, hungerOk), new FuzzyTerm(desirable));
        RLDesire.AddRule(new FzAND(targetMedium, hungerLow), new FuzzyTerm(undesirable));//
        RLDesire.AddRule(new FzAND(targetClose, hungerHigh), new FuzzyTerm(veryDesirable));
        RLDesire.AddRule(new FzAND(targetClose, hungerOk), new FuzzyTerm(veryDesirable));//
        RLDesire.AddRule(new FzAND(targetClose, hungerLow), new FuzzyTerm(undesirable));//
    }

    void Update()
    {
        //Calculate desirability every frame
        CalculateDesirability();
    }

    void CalculateDesirability()
    {
        //Firstly, fuzzify our two antecedents
        RLDesire.Fuzzify("DistanceToTarget", closestFood);
        RLDesire.Fuzzify("Hunger", hunger);

        //this would create our confidence values by evaulating against the ruleset
        //and store them in the consequent FuzzyVariable (which should be Desirability)
        RLDesire.FuzzyInference();

        //Get our crisp output.
        crispDesirability = RLDesire.Defuzzify("Desirability", FuzzyModule.DefuzzifyType.Max_Av);
    }
}
