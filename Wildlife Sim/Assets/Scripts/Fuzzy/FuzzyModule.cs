﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the main class where the whole Fuzzy Process comes together. Every AI agent that will 
/// make use of Fuzzy Logic will need to instantiate one of these
/// </summary>
public class FuzzyModule {

    public enum DefuzzifyType
    {
        Max_Av,
        //We could add other Defuzzify types here
    };

    //All FLVs will be stored in this map
    public Dictionary<string, FuzzyVariable> FLVMap = new Dictionary<string, FuzzyVariable>();

    //Ruleset will be stored in here
    public List<FuzzyRule> ruleList = new List<FuzzyRule>();

    //Will create an FLV, store it in our map and return it
    public FuzzyVariable CreateFLV(string FLV_Name)
    {
        FuzzyVariable theFLV = new FuzzyVariable();

        FLVMap.Add(FLV_Name, theFLV);

        return theFLV;
    }

    //Adds a rule to this module
    public void AddRule(FuzzyTerm antecedent, FuzzyTerm consequence)
    {
        //You have to write the class FuzzyRule!
        FuzzyRule fr = new FuzzyRule(antecedent, consequence);
        ruleList.Add(fr);
    }

    /// <summary>
    /// Run through every Rule in the rule set, generating a confidence value for each rule
    /// </summary>
    public void FuzzyInference()
    {
        //You have to implement this part yourself!
        foreach (FuzzyRule fr in ruleList)
        {
            fr.consequent.AddDOM(fr.GetConfidence());
        }
    }

    //Fuzzifies a value against a FLV, setting the DOMs for all Fuzzy sets in that FLV
    public void Fuzzify(string FLV_Name, float val)
    {
        FuzzyVariable FLV;
        if (FLVMap.TryGetValue(FLV_Name, out FLV))
        {
            FLV.Fuzzify(val);
        }
    }

    //Returns the defuzzified value of a set using the specified method
    public float Defuzzify(string FLV_Name, DefuzzifyType method = DefuzzifyType.Max_Av)
    {
        float rv = 0.0f;

        if (method == DefuzzifyType.Max_Av)
        {
            FuzzyVariable FLV;
            if (FLVMap.TryGetValue(FLV_Name, out FLV))
            {
                //You have to implement this part yourself
                rv = FLV.DefuzzifyMaxAv(); // this method is not implemented!
            }
        }

        return rv;

    }
}
