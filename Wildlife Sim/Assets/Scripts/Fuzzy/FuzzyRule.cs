﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A fuzzy rule is a container for two FuzzyTerms: an antecedent and consequent.
/// </summary>
public class FuzzyRule
{
    public FuzzyTerm antecedent, consequent;
    public FuzzyRule (FuzzyTerm antecedentT, FuzzyTerm consequentT)
    {
        antecedent = antecedentT;
        consequent = consequentT;
    }

    public float GetConfidence()
    {
        return antecedent.GetDOM();
    }

}
