﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// These classes can combine FuzzySets into the antecedent and consequent parts (so we can AND and OR them correctly)
/// This is essential for defining your ruleset.
/// </summary>
public class FuzzyTerm
{
    FuzzySet set;

    public FuzzyTerm(FuzzySet v1)
    {
        set = v1;
    }

    public virtual void AddDOM(float val)
    {
        set.DOMs.Add(val);
    }
    public virtual float GetDOM()
    {
        return set.DOM;
    }

    public virtual void SetDOM(float val)
    {
        set.DOM = val;
    }
}
public class FzAND : FuzzyTerm
{
    List<FuzzySet> sets = new List<FuzzySet>();

    //ANDs two Fuzzy sets
    public FzAND(FuzzySet v1, FuzzySet v2) : base(v1)
    {
        sets.Add(v1);
        sets.Add(v2);
    }

    //ANDs three Fuzzy sets
    public FzAND(FuzzySet v1, FuzzySet v2, FuzzySet v3) : base(v1)
    {
        sets.Add(v1);
        sets.Add(v2);
        sets.Add(v3);
    }

    //ANDs four Fuzzy sets
    public FzAND(FuzzySet v1, FuzzySet v2, FuzzySet v3, FuzzySet v4) : base(v1)
    {
        sets.Add(v1);
        sets.Add(v2);
        sets.Add(v3);
        sets.Add(v4);
    }

    public override float GetDOM()
    {
        float LowestDOM = 9999.0f;

        foreach (FuzzySet f in sets)
        {
            if (f.DOM < LowestDOM)
                LowestDOM = f.DOM;
        }
        return LowestDOM;
    }
}
