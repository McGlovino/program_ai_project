﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This represents an FLV (Fuzzy Linguistic Variable). This contains our sets
/// </summary>
public class FuzzyVariable {

    public Dictionary<string, FuzzySet> memberSets = new Dictionary<string, FuzzySet>();

    //Min and Max ranges are adjusted as we add more member sets
    float minRange, maxRange;
    private void AdjustRangeToFit(float min, float max)
    {
        if (min < minRange)
            minRange = min;

        if (max > maxRange)
            maxRange = max;
    }

    public FuzzyVariable()
    {
        minRange = 0.0f;
        maxRange = 0.0f;
    }

    /// <summary>
    /// This method adds a triangular set to this FLV
    /// </summary>
    public FuzzySet AddTriangularSet(string name, float minBound, float peak, float maxBound)
    {
        FuzzySet fs = new FuzzySet_Triangle(peak, minBound, maxBound);
        memberSets.Add(name, fs);
        AdjustRangeToFit(minBound, maxBound);
        return fs;
    }

    /// <summary>
    /// This method adds a left shoulder set to this FLV
    /// </summary>
    public FuzzySet AddLeftShoulderSet(string name, float minBound, float peak, float maxBound)
    {
        FuzzySet fs = new FuzzySet_LeftShoulder(peak, minBound, maxBound);
        memberSets.Add(name, fs);
        AdjustRangeToFit(minBound, maxBound);
        return fs;
    }

    /// <summary>
    /// This method adds a right shoulder set to this FLV
    /// </summary>
    public FuzzySet AddRightShoulderSet(string name, float minBound, float peak, float maxBound)
    {
        FuzzySet fs = new FuzzySet_RightShoulder(peak, minBound, maxBound);
        memberSets.Add(name, fs);
        AdjustRangeToFit(minBound, maxBound);
        return fs;
    }

    /// <summary>
    /// Fuzzifies a value against every single set in this FLV, storing the DOM within each set
    /// </summary>
    public void Fuzzify(float val)
    {
        foreach (KeyValuePair<string, FuzzySet> fs in memberSets)
            fs.Value.DOM = fs.Value.CalculateDOM(val);
    }

    public float DefuzzifyMaxAv()
    {
        float defuzzified = 0;
        float sumOfConfidence = 0;
        foreach (KeyValuePair<string, FuzzySet> k in memberSets)
        {
            float confidence = 0;
            foreach(float f in k.Value.DOMs)
            {
                if (f > confidence)
                    confidence = f;
            }
            
            defuzzified += ((k.Value.RepresentativeValue) * confidence);
            
            sumOfConfidence += confidence;

            k.Value.DOMs.Clear(); // clear for next run
        }
        if (sumOfConfidence == 0)
            return 0;
        float desirability = defuzzified / sumOfConfidence;

        return desirability;
    }
}
