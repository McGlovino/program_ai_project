﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockingManager : MonoBehaviour
{
    GameObject fishPrefab;
    public int numFish;
    public GameObject[] allFish;
    public Vector3 swimLimit;
    //public int swimLimit = 5;
    public int numOfGoals;
    public Vector3[] goalPos;
    public float minSpeed;
    public float maxSpeed;
    public float neighbourDistance = 2f;
    public float rotSpeed = 1.4f;


    void Start()
    {
        swimLimit = new Vector3(transform.localScale.x, 0, transform.localScale.z) * 1.6f;

        fishPrefab = (GameObject)Resources.Load("Prefabs/Fish");
        goalPos = new Vector3[numOfGoals];
        allFish = new GameObject[numFish];
        for (int i = 0; i < allFish.Length; i++)
        {
            Vector3 pos = transform.position + new Vector3(Random.Range(-swimLimit.x, swimLimit.x),
                                swimLimit.y,
                                Random.Range(-swimLimit.z, swimLimit.z));

            allFish[i] = (GameObject)Instantiate(fishPrefab, pos, Quaternion.identity);
            allFish[i].GetComponent<Flock>().myManager = this;
        }
        UpdateGoalPos();
    }

    void Update()
    {
        if (Random.Range(0, 100) < 9)
            UpdateGoalPos();
    }

    private void UpdateGoalPos()
    {
        for (int i = 0; i < goalPos.Length; i++)
        {
            goalPos[i] = transform.position + new Vector3(Random.Range(-swimLimit.x, swimLimit.x),
                                swimLimit.y,
                                Random.Range(-swimLimit.z, swimLimit.z));
        }

        foreach (GameObject g in allFish)
            g.GetComponent<Flock>().FindClosestGoalPos();
    }
}
