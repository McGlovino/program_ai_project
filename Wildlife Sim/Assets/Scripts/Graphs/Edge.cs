﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge
{
    public Node fromNode;
    public Node toNode;

    public Edge(Node from, Node to)
    {
        fromNode = from;
        toNode = to;
    }
}
