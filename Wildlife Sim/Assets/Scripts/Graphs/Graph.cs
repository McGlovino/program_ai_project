﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph
{
    List<Edge> edges;
    public List<List<Node>> nodes;
    public List<Node> pathList;

    bool limitSearch = true;
    int searchCount;

    public Graph()
    {
        edges = new List<Edge>();
        nodes = new List<List<Node>>();
        pathList = new List<Node>();
    }

    public Graph Deepcopy()
    {
        Graph temp = new Graph();
        temp.edges = edges;
        temp.nodes = nodes;
        return temp;
    }

    public void AddEdge(GameObject fromNode, GameObject toNode)
    {
        Node from = FindNode(fromNode);
        Node to = FindNode(toNode);

        if (from != null && to != null)
        {
            Edge e = new Edge(from, to);
            edges.Add(e);
            from.edgelist.Add(e);
        }
  }
    public void AddEdge(Node fromNode, Node toNode)
    {
        Node from = fromNode;
        Node to = toNode;

        if (from != null && to != null)
        {
            Edge e = new Edge(from, to);
            edges.Add(e);
            from.edgelist.Add(e);
        }
    }

    public Node FindNode(GameObject compareObject)
    {
        foreach (List<Node> i in nodes)
        {
            foreach (Node j in i)
            {
                if (j.gObject == compareObject)
                    return j;
            }
        }
        return null;
    }
    public Node FindNode(Vector2Int find)
    {
        if (find.x >= 0 && find.y >= 0 && find.x < nodes.Count)
        {
            if (find.y < nodes[find.x].Count)
                return nodes[find.x][find.y];
            else
                return null;
        }
        else
            return null;
    }

    public int getPathLength()
    {
        return pathList.Count;
    }
    public GameObject getPathPoint(int index)
    {
        return pathList[index].gObject;
    }
    public void CreateAStarGrid()
    {
        foreach (List<Node> i in nodes)
        {
            foreach (Node j in i)
            {
                if (j.canWalk > 0)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        Node nodeTemp = FindNode(j.direction(k));
                        if (nodeTemp != null && nodeTemp.canWalk > 0)
                            AddEdge(j, nodeTemp);
                    }
                }
            }
        }
    }

    ///manhattan
    float distance2(Node a, Node b)
    {
        float dx = a.xPos - b.xPos;
        float dy = a.yPos - b.yPos;
        float dist = dx * dx + dy * dy;
        return (dist);
    }
    ///euclidian
    float distance(Node a, Node b)
    {
        return ((a.gObject.transform.position - b.gObject.transform.position).magnitude);
    }
    public bool AStar(GameObject startObj, GameObject endObj)
    {
        searchCount = 0;

        Node start = FindNode(startObj);
        Node end = FindNode(endObj);

        if (start == null || end == null)
            return false;

        List<Node> open = new List<Node>();
        List<Node> closed = new List<Node>();
        bool tentative;

        start.g = 0;
        start.h = distance(start, end);
        start.f = 0.0f;
        open.Add(start);

        while (open.Count > 0)
        {
            searchCount++;
            if (limitSearch && searchCount > 400)
            {
                //Debug.Log("Search Limit reached");
                return false;
            }

            int i = lowestF(open);
            Node thisnode = open[i];

            open.RemoveAt(i);
            closed.Add(thisnode);

            Node neighbour;
            foreach (Edge e in thisnode.edgelist)
            {
                neighbour = e.toNode;
                //if its in the closed list skip this neighbor
                if (closed.IndexOf(neighbour) > -1)
                    continue;

                neighbour.h = distance(neighbour, end);
                neighbour.g = thisnode.g + neighbour.h + (distance(thisnode, neighbour) / neighbour.canWalk);
                neighbour.f = neighbour.g + neighbour.h;

                //add it to open
                if (open.IndexOf(neighbour) == -1)
                {
                    open.Add(neighbour);
                    tentative = true;
                }
                else if (thisnode.f < neighbour.g)
                    tentative = true;
                else
                    tentative = false;

                if (tentative)
                {
                    neighbour.cameFrom = thisnode;
                    neighbour.f = neighbour.g + neighbour.h;
                }
                //path found
                if (thisnode.gObject == endObj)
                {
                    reconstructPath(start, end);
                    return true;
                }
            }
        }
        return false;
    }

    int lowestF(List<Node> l)
    {
        float lowestf = 0;
        int count = 0;
        int iteratorCount = 0;

        for (int i = 0; i < l.Count; i++)
        {
            if (i == 0)
            {
                lowestf = l[i].f;
                iteratorCount = count;
            }
            else if (l[i].f <= lowestf)
            {
                lowestf = l[i].f;
                iteratorCount = count;
            }
            count++;
        }
        return iteratorCount;
    }

    public void reconstructPath(Node startNode, Node endNode)
    {
        pathList.Clear();
        pathList.Add(endNode);

        var p = endNode.cameFrom;
        while (p != startNode && p != null)
        {
            pathList.Insert(0, p);
            p = p.cameFrom;
        }
        pathList.Insert(0, startNode);
    }

    public void debugDraw()
    {
        //draw edges
        for (int i = 0; i < edges.Count; i++)
        {
            Debug.DrawLine(edges[i].fromNode.gObject.transform.position, 
                edges[i].toNode.gObject.transform.position, edges[i].toNode.canWalk < 1 ? Color.red : Color.green);

        }
        //draw directions
        for (int i = 0; i < edges.Count; i++)
        {
            Vector3 to = (edges[i].fromNode.gObject.transform.position - edges[i].toNode.gObject.transform.position) * 0.05f;
            Debug.DrawRay(edges[i].toNode.gObject.transform.position, to, Color.blue);
        }
    }

    public GameObject randomInRadius(Vector3 pos, float radius)
    {
        Collider[] inRadius = Physics.OverlapSphere(pos, radius, LayerMask.GetMask("Floor"));
        return (inRadius[Random.Range(0, inRadius.Length-1)].gameObject);
    }
}
