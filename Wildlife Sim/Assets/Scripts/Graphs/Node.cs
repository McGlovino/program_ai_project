﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public List<Edge> edgelist = new List<Edge>();
    public GameObject gObject;
    public float xPos, yPos;
    public float f, g, h;
    public Node cameFrom;
    public Vector2Int Gridpos;
    //public bool active = false;
    //public List<GameObject> worldDetail = new List<GameObject>();
    public bool objectOn;
    public float canWalk;

    public Node(GameObject gObjectT, int xT, int yT, float canWalkT)
    {
        objectOn = false;
        canWalk = canWalkT;
        gObject = gObjectT;
        Gridpos = new Vector2Int(xT, yT);
        xPos = gObjectT.transform.position.x;
        yPos = gObjectT.transform.position.y;
    }

    private static List<Vector2Int> directions = new List<Vector2Int>(){
        new Vector2Int(1, 0), new Vector2Int(0, -1),
        new Vector2Int(-1, 0), new Vector2Int(0, 1),
        // diagonals
        new Vector2Int(1, 1), new Vector2Int(1, -1),
        new Vector2Int(-1, 1), new Vector2Int(-1, -1),
    };

    public Vector2Int direction(int direction)//0-7
    {
        if (direction <= 7 && direction >= 0)
        {
            return new Vector2Int((Gridpos.x + directions[direction].x), (Gridpos.y + directions[direction].y));
        }
        else
            return new Vector2Int(9999,9999);
    }
}